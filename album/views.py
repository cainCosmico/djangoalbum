from django.shortcuts import render
from django.http import HttpResponse
from album.models import Category, Photo
from django.views.generic import ListView, DetailView
#from django.core.urlresolvers import reverse_lazy
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

# HttpRequest, en el que va la información referente a la solicitud
# que estamos haciendo, como por ejemplo si el método empleado es POST o GET


def first_view(request):
    return HttpResponse('Esta es mi primera vista!')

def category(request):
    category_list = Category.objects.all()
    context = {'object_list': category_list}
    return render(request, 'album/category.html', context)


def category_detail(request, category_id):
    category = Category.objects.get(id=category_id)
    context = {'object': category}
    return render(request, 'album/category_detail.html', context)

# Vistas basadas en clases 
class PhotoListView(LoginRequiredMixin, ListView):
    model = Photo

class PhotoDetailView(DetailView):
    model = Photo

# Template base 
# Aqui se proteje todo con contraseña
@login_required(login_url='/accounts/login/')
def base(request):
    return render(request, 'base.html')

class PhotoUpdate(LoginRequiredMixin, UpdateView):
    model = Photo
    fields = "__all__"
    success_url = reverse_lazy('photo-list')

class PhotoCreate(LoginRequiredMixin, CreateView):
    model = Photo
    fields = "__all__"
    success_url = reverse_lazy('photo-list')

class PhotoDelete(LoginRequiredMixin, DeleteView):
    model = Photo
    success_url = reverse_lazy('photo-list')
