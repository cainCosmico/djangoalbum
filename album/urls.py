# from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from django.urls import path, include
from album import views

urlpatterns = [
    path('', views.base, name='base'),
    path('vista', views.first_view, name='first-view'),
    path('category/', views.category, name='category-list'),
    path('category/<int:category_id>/detail/', views.category_detail, name='category-detail'),
    
    path('photo/', views.PhotoListView.as_view(), name='photo-list'),
    # Aqui se proteje con contraseña y usuario, por URL
    path('photo/<int:pk>/detail/', login_required(views.PhotoDetailView.as_view()), name='photo-detail'),

    # Update
    path('photo/<int:pk>/update/', views.PhotoUpdate.as_view(), name='photo-update'),    
    # Create     
    path('photo/create/', views.PhotoCreate.as_view(), name='photo-create'),
    # Delete
    path('photo/<int:pk>/delete/', views.PhotoDelete.as_view(), name='photo-delete'),
]

# http://127.0.0.1:8000/album/category/2/detail/
# http://127.0.0.1:8000/album/photo/2/detail/
