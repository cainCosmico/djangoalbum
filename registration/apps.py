from django.apps import AppConfig


class RegistrationConfig(AppConfig):
    name = 'registration'

class CoreConfig(AppConfig):
    name = 'album'